CREATE TABLE server (
       id INTEGER PRIMARY KEY AUTOINCREMENT,
       name TEXT NOT NULL,
       crypto_idfp TEXT NOT NULL,
       server_group INTEGER,
       is_active INTEGER NOT NULL
) STRICT;

CREATE TABLE record (
       id INTEGER PRIMARY KEY AUTOINCREMENT,
       nickname TEXT NOT NULL,
       nickname_nocolors TEXT NOT NULL,
       crypto_idfp TEXT,
       ip TEXT,
       map TEXT NOT NULL,
       server_id INTEGER NOT NULL REFERENCES server(id),
       time INTEGER NOT NULL,
       timestamp INTEGER NOT NULL,
       cp_times TEXT NOT NULL,
       top_speed REAL NOT NULL,
       start_speed REAL NOT NULL,
       average_speed REAL NOT NULL,
       strafe_percentage REAL NOT NULL
) STRICT;

CREATE INDEX record__nickname_nocolors ON record(nickname_nocolors);
CREATE INDEX record__crypto_idfp ON record(crypto_idfp);
CREATE INDEX record__ip ON record(ip);
CREATE INDEX record__map ON record(map);
CREATE INDEX record__time ON record(time);
