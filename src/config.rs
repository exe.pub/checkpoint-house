use std::net::SocketAddr;

use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub db_file: String,
    pub secret_key: String,
    pub listen_on: SocketAddr,
}
