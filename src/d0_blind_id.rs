use crate::d0_blind_id_sys::*;
use std::os::raw::c_char;

const KEY_0_D0PK: &[u8] = include_bytes!("../key_0.d0pk");

const FOURCC_D0PK: u32 = (b'd' as u32)
    | ((b'0' as u32) << 8)
    | ((b'p' as u32) << 16)
    | ((b'k' as u32) << 24);

const fn little_long(data: &[u8]) -> u32 {
    (data[0] as u32)
        | ((data[1] as u32) << 8)
        | ((data[2] as u32) << 16)
        | ((data[3] as u32) << 24)
}

fn parse_pack(buf: &[u8], header: u32, nlumps: usize) -> Option<Vec<Vec<u8>>> {
    let mut pos = 0;
    if header > 0 {
        if buf.len() < 4 {
            return None;
        }
        if little_long(buf) != header {
            return None;
        }
        pos += 4;
    }
    let mut lumps = Vec::with_capacity(nlumps);
    for _i in 0..nlumps {
        if pos + 4 > buf.len() {
            return None;
        }
        let s = little_long(&buf[pos..pos + 4]) as usize;
        pos += 4;
        if pos + s > buf.len() {
            return None;
        }
        let lump: Vec<_> = Vec::from(&buf[pos..pos + s]);
        lumps.push(lump);
        pos += s;
    }
    Some(lumps)
}

pub fn d0_init() {
    unsafe {
        d0_blind_id_INITIALIZE();
    }
}

pub fn is_signature_valid(data: &[u8], sig: &[u8]) -> Option<String> {
    let lumps = parse_pack(KEY_0_D0PK, FOURCC_D0PK, 2).unwrap();
    unsafe {
        let ctx = d0_blind_id_new();
        if d0_blind_id_read_public_key(
            ctx,
            lumps[0].as_ptr() as *const c_char,
            lumps[0].len(),
        ) == 0
        {
            panic!("Could not read public key")
        };
        if d0_blind_id_read_private_id_modulus(
            ctx,
            lumps[1].as_ptr() as *const c_char,
            lumps[1].len(),
        ) == 0
        {
            panic!("Could not decode modulus")
        }
        let mut status = 0;
        let res = d0_blind_id_sign_with_private_id_verify_detached(
            ctx,
            1,
            0,
            sig.as_ptr() as *const c_char,
            sig.len(),
            data.as_ptr() as *const c_char,
            data.len(),
            &mut status,
        );
        if res == 0 {
            return None;
        }
        let mut outbuf = [0; 513];
        let mut outbuflen = 512;
        let res = d0_blind_id_fingerprint64_public_id(
            ctx,
            outbuf.as_mut_ptr(),
            &mut outbuflen,
        );
        if res == 0 {
            return None;
        }
        let bytes: Vec<u8> =
            outbuf[0..outbuflen].iter().map(|x| *x as u8).collect();
        match String::from_utf8(bytes) {
            Ok(s) => Some(s),
            Err(_) => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sig_verify() {
        let data = b"&submit=foo\0";
        let sig = "gQEBTl/CKUO7kU3WZcRS2pB4SRho1Za2SLfHjp82OHDnLyLoEo5anCuuFB1HcQyzjSpoIMbue2A/+hwcrwSv0TIIDfjfMDQbCDYEh+PAQx4792g0dFlE/oarZEQ5nR9CrvIcgjWuGZjOfEQtIkEd8oH5muav253iOPtV/4d/OAy7GHSBAQGPqnGoD6GhuHLYN+Sf73ROColneBdJ7ttuVwm32FvI8LuD5aLDll7bpqfHTWhgbTW02CYvkTAYtoz2RZmIGK5ZHHaM/V6vcSXnq2ab/7mFRiag7D5OUsmIFY9E3IqcqtP7+wXSVgiNFY3DBPy27bXjk8ZJ9nUD5dQBL9sG8TzWd4EBAes813+fuONKhyW5CzsGPDveWZar9LftMzIEEJe+jd8p7RqaM720qEHSeEfUrH6oStCJhTiaPrgo5bc8p+nMe/KRHtWbr2Cij515hgefv9FruWlzJ6EqAQSjmiTsHCANE9bYGvw85+l6RKcnmaPw1kl9gV92yLcWBtptvLm+E8NzgQEBFvjKwIbo2kiOpRmfjnO7RuI7Q+RY6gXuNy+/8VdqkhxuuY6uuJ25HstRuh3Ujsh175wzoHRLn+m4NPAPKxsW3lASb2Ne8a4PRZnWVWQcQIKKfGMTaTfl5bhWY7f4FhPmnXDrqw9r+9mqYtKkWzojGDj8B88hU0oVdR6I3HELFWI=";
        d0_init();
        assert_eq!(
            is_signature_valid(data, &base64::decode(sig).unwrap()).unwrap(),
            String::from("dclo9XPmaARYeURLBRUj4aIVhE9oqUvj98rRaCueZkA=")
        )
    }
}
