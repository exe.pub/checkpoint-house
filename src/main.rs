mod config;
mod d0_blind_id;
mod d0_blind_id_sys;

use actix_web::{
    http::header::ContentType, http::StatusCode, middleware::Logger, post, web,
    App, HttpRequest, HttpResponse, HttpServer,
};
use base64::{engine::general_purpose, Engine as _};
use chrono::prelude::*;
use clap::Parser;
use dpcolors::DPString;
use sqlx::{sqlite::SqlitePoolOptions, SqlitePool};
use std::net::AddrParseError;
use std::net::IpAddr;
use std::num::{ParseFloatError, ParseIntError};
use std::path::PathBuf;
use thiserror::Error;

const D0_SIG_HEADER: &str = "x-d0-blind-id-detached-signature";

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long, default_value = "./config.toml")]
    config: PathBuf,
}

#[derive(Debug, Error)]
enum WebError {
    #[error("Unauthorized")]
    Unauthorized,
    #[error("Not Found")]
    NotFound(String),
    #[error("DB Error: {0}")]
    DbError(#[from] sqlx::Error),
    #[error("Base 64 Error: {0}")]
    Base64Error(#[from] base64::DecodeError),
    #[error("Anyhow Error: {0}")]
    AnyhowError(#[from] anyhow::Error),
    #[error("Chrono Parse Error: {0}")]
    ChronoParseError(#[from] chrono::ParseError),
    #[error("Parse int error")]
    ParseIntError(#[from] ParseIntError),
    #[error("Parse float error")]
    ParseFloatError(#[from] ParseFloatError),
    #[error("Ip addr parse error")]
    ParseIpError(#[from] AddrParseError),
    #[error("Invalid Payload")]
    InvalidPayload,
    #[error("Invalid Payload Version")]
    InvalidPayloadVersion,
}

impl actix_web::error::ResponseError for WebError {
    fn error_response(&self) -> HttpResponse {
        let body = match self {
            WebError::NotFound(_) => "NOT FOUND",
            WebError::Unauthorized => "UNAUTHORIZED",
            _ => "INTERNAL ERROR",
        };
        HttpResponse::build(self.status_code())
            .insert_header(ContentType::plaintext())
            .body(body)
    }

    fn status_code(&self) -> StatusCode {
        match self {
            WebError::Unauthorized => StatusCode::UNAUTHORIZED,
            WebError::NotFound(_) => StatusCode::NOT_FOUND,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum PayloadVersion {
    V1,
}

impl PayloadVersion {
    fn from_request(line: &str) -> Result<Self, WebError> {
        match line {
            "RECORDv1" => Ok(PayloadVersion::V1),
            _ => Err(WebError::InvalidPayloadVersion),
        }
    }
}

#[derive(Debug)]
struct Payload {
    version: PayloadVersion,
    timestamp: DateTime<Utc>,
    time: u32,
    checkpoints: [u32; 255],
    nickname: DPString,
    map: String,
    crypto_idfp: Option<String>,
    ip: Option<IpAddr>,
    start_speed: f64,
    top_speed: f64,
    average_speed: f64,
    strafe_percentage: f64,
}

impl Payload {
    fn from_request(data: &str) -> Result<Payload, WebError> {
        let lines: Vec<_> = data.splitn(12, '\n').collect();
        if lines.len() != 12 {
            return Err(WebError::InvalidPayload);
        }
        let mut cps = [0; 255];
        if !lines[6].is_empty() {
            for cp in lines[6].split(';') {
                if let Some((cpn, time)) = cp.split_once(' ') {
                    cps[cpn.parse::<usize>()?] = time.parse()?;
                } else {
                    return Err(WebError::InvalidPayload);
                }
            }
        }

        Ok(Payload {
            version: PayloadVersion::from_request(lines[0])?,
            timestamp: lines[1].parse::<DateTime<Utc>>()?,
            map: String::from(lines[2]),
            crypto_idfp: if lines[3].is_empty() {
                None
            } else {
                Some(String::from(lines[3]))
            },
            ip: lines[4].parse().ok(),
            time: lines[5].parse()?,
            checkpoints: cps,
            top_speed: lines[7].parse()?,
            average_speed: lines[8].parse()?,
            start_speed: lines[9].parse()?,
            strafe_percentage: lines[10].parse()?,
            nickname: DPString::parse(lines[11]),
        })
    }

    async fn save_to_db(
        &self,
        pool: &SqlitePool,
        server_id: i64,
    ) -> Result<(), WebError> {
        let mut cps = Vec::new();
        for (cpn, cpt) in self.checkpoints.iter().enumerate() {
            if *cpt > 0 {
                cps.push(format!("{cpn} {cpt}"));
            }
        }
        let cps = cps.join(";");
        let ts = self.timestamp.timestamp();
        let ip = self.ip.as_ref().map(|i| format!("{}", i));
        let n = self.nickname.to_dp();
        let nn = self.nickname.to_none();
        sqlx::query!(r#"INSERT INTO record
(nickname, nickname_nocolors, crypto_idfp, ip, map, server_id, time, timestamp, cp_times, top_speed, start_speed, average_speed, strafe_percentage)
VALUES
(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
"#,
                     n,
                     nn,
                     self.crypto_idfp,
                     ip,
                     self.map,
                     server_id,
                     self.time,
                     ts,
                     cps,
                     self.top_speed,
                     self.start_speed,
                     self.average_speed,
                     self.strafe_percentage
        ).execute(pool).await?;
        Ok(())
    }
}

async fn get_server(pool: &SqlitePool, fp: &str) -> Result<i64, WebError> {
    let server = sqlx::query!(
        "SELECT id FROM server WHERE crypto_idfp=? AND is_active=1",
        fp
    )
    .fetch_one(pool)
    .await?;
    Ok(server.id)
}

#[post("/new-record")]
async fn post_record(
    request: HttpRequest,
    payload: web::Bytes,
    pool: web::Data<SqlitePool>,
) -> Result<HttpResponse, WebError> {
    if let Some(sig) = request.headers().get(D0_SIG_HEADER) {
        let decoded_sig = general_purpose::STANDARD.decode(sig)?;
        let mut d = Vec::with_capacity(payload.len() + 1);
        for i in payload.into_iter() {
            d.push(i);
        }
        d.push(b'\0');
        if let Some(fp) = d0_blind_id::is_signature_valid(&d, &decoded_sig) {
            println!("SERVER FINGERPRINT: {fp}");
            let server_id = get_server(&pool, &fp).await?;
            match Payload::from_request(&String::from_utf8_lossy(&d)) {
                Ok(payload) => {
                    payload.save_to_db(&pool, server_id).await?;
                    Ok(HttpResponse::build(StatusCode::CREATED)
                       .insert_header(ContentType::plaintext())
                       .body("OK\n"))
                }
                Err(e) => {
                    println!("ERROR DURING PROCESSING PAYLOAD {e}: {}", String::from_utf8_lossy(&d));
                    Err(e)
                }
            }
        } else {
            Err(WebError::Unauthorized)
        }
    } else {
        Err(WebError::Unauthorized)
    }
}

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    pretty_env_logger::init();
    let args = Args::parse();
    let raw_config = std::fs::read_to_string(&args.config)?;
    let config: config::Config = toml::from_str(&raw_config)?;
    let pool = SqlitePoolOptions::new().connect(&config.db_file).await?;
    sqlx::migrate!().run(&pool).await?;
    let d_pool = web::Data::new(pool);
    d0_blind_id::d0_init();
    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .app_data(d_pool.clone())
            .service(post_record)
    })
    .bind(config.listen_on)?
    .workers(4)
    .run()
    .await?;
    Ok(())
}
