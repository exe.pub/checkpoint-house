use std::env;
use std::path::PathBuf;

use bindgen::CargoCallbacks;

fn main() {
    // This is the directory where the `c` library is located.
    let libdir_path = PathBuf::from("d0_blind_id")
        // Canonicalize the path as `rustc-link-search` requires an absolute
        // path.
        .canonicalize()
        .expect("cannot canonicalize path");

    // This is the path to the `c` headers file.
    let headers_path = libdir_path.join("d0_blind_id.h");
    let headers_path_str = headers_path.to_str().expect("Path is not a valid string");

    // Tell cargo to look for shared libraries in the specified directory
    println!("cargo:rustc-link-search={}/.libs", libdir_path.to_str().unwrap());

    println!("cargo:rustc-link-lib=static=d0_blind_id");
    println!("cargo:rustc-link-lib=static=d0_rijndael");
    println!("cargo:rustc-link-lib=gmp");
    // Tell cargo to invalidate the built crate whenever the header changes.
    println!("cargo:rerun-if-changed={}", headers_path_str);

    env::set_current_dir(&libdir_path).expect("couldn't change dir");

    // Run `clang` to compile the `hello.c` file into a `hello.o` object file.
    // Unwrap if it is not possible to spawn the process.
    let output = std::process::Command::new("bash")
        .arg("-c")
        .arg("./autogen.sh && ./configure && make")
        .output()
        .expect("could not spawn d0_blind_id compilation");
    println!("{}", String::from_utf8_lossy(&output.stdout));
    println!("{}", String::from_utf8_lossy(&output.stderr));
    if !output.status.success()
    {
        // Panic if the command was not successful.
        panic!("could not compile object file");
    }
    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .header(headers_path_str)
        // Tell cargo to invalidate the built crate whenever any of the
        // included header files changed.
        .parse_callbacks(Box::new(CargoCallbacks))
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap()).join("d0_blind_id_sys.rs");
    bindings
        .write_to_file(out_path)
        .expect("Couldn't write bindings!");
}
